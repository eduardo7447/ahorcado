package ahor;

public class Ahorcado {
	private String palClave;
	private char c;
	private String aux;
	private int intentos = 0 ;
	
	public Ahorcado(String palabraSecreta) {
		this.palClave = palabraSecreta;
		
	}
	
	public void auxiliar(String palabraSecreta) {
		this.aux= "";
		for (int i = 0 ; i < palClave.length() ; i++) {
			this.aux =this.aux+"-";
		}
	}
	
	public int intentosRealizados() {
		return this.intentos;
	}
	
	 /*funciona excepto cuando la primera letra ingresada no esta en la palabra secreta
	  * 
	  */
	public void cambiarApariciones() {
		String aux2 = "";
		if (arriesgarLetra(this.c)) {
			
			for (int i = 0 ; i < palClave.length() ; i++) {
				if(this.palClave.charAt(i) != this.c) {
					aux2=aux2+this.aux.charAt(i);
				}else {
					aux2= aux2+this.c;

				}
			}
			
		}
		
		this.aux = aux2;
	}
	
	/* la letra ingresada esta en la palabra clave retorna true la variable de instancia se le asigna 
	 * 
	 * el valor del char ingresado
	 * 
	 * En el caso que no se acierte se incrementa en 1 los intentos realizados y se retorna false;
	 * 
	 */
	public boolean arriesgarLetra(char letra) {
		
		for (int i = 0 ; i < palClave.length() ; i++) {
			if(letra == this.palClave.charAt(i)) {
				this.c = letra;
				return true;
			}
		}
		this.intentos++;
		return false;
	}
	/* se arriesga una palabra y se asigna al valor auxiliar de la variable de instancia
	 *  * 
	 */
	public void arriesgarPalabra(String s) {
		this.aux = s;
	
	}
	/*Compara si la variable de instancia aux == a la palabraClave si esto coincide retorna verdadero
	 * 
	 * en caso contrario false; 
	 */
	public boolean ganado() {
		return this.aux.equals(this.palClave) ;
	}
	
	public void mostrar() {
		
		//auxiliar(this.palClave);
		
		if(this.aux.length()< 1) {
			return;
		}
		
		for (int i = 0 ; i < palClave.length() ; i++) {
			System.out.print(this.aux.charAt(i));
		}
		System.out.println();

	}
}
